@extends('layouts.app')
@section('title')
 Generar Citas   
@endsection
@section('contenido')

<div class="page-title">
    <div class="title_left">
      <h3>Generar Cita</h3>
    </div>    
</div>
<div class="x_panel">
    <div class="x_title">
      <h2>Paciente <small></small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li><a class="close-link "><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix "></div>
    </div>
    <div class="x_content">
      <br />
      
      <form class="form-horizontal form-label-left input_mask" method="POST" action="/paciente">
        {!! csrf_field() !!}
        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
          <span class="fa fa-user form-control-feedback left"  aria-hidden="true"></span>
        </div>

        

        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
          <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
            <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
          <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
            <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
            <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
          <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
          <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
        </div>
       
        <div class="ln_solid"></div>
        <div class="panel-body">
            
            <div class="x_panel">
                    <div class="x_title">
                      <h2>Paciente... <small>datos complementarios</small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content row">
                      
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                          <input type="text" class="form-control has-feedback-left" id="inputSuccess2" placeholder="Lugar de Nacimiento" name="lugarnac">
                          <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                        </div>
            
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                          <input type="text" class="form-control" id="inputSuccess3" placeholder="Lugar de Procedencia" name="lugarproc">
                          <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
            
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                          <input type="text" class="form-control has-feedback-left" id="inputSuccess4" placeholder="Correo Electrónico" name="email">
                          <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                        </div>
            
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            
                        <input type="text" class="form-control hidden" id="inputSuccess5" placeholder="Personas_id" name="personas_id" value="">
                          <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
                        </div>
            
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                          <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                        </div>
            
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                          <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
                       
                        <div class="ln_solid"></div>
                        <div class="form-group">
                          <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                            
                             <button class="btn btn-primary" type="reset">Limpiar</button>
                             
                            <button type="submit" class="btn btn-success" href="/cita/create?doc={{$_GET['doc']}}" >Guardar</button>
                          </div>
                        </div>
            
                      </form>
                    </div>
                  </div>

    </div>

     
    </div>
  </div>
      <!-- end of accordion -->


    </div>
  </div>
</div>

  <div class="x_panel">
        <div class="x_title">
          <h2>Cita Médica <small>...</small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          
          <form class="form-horizontal form-label-left input_mask" method="POST" action="/cita">
            {!! csrf_field() !!}

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
              <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
              <input type="text" class="form-control" id="inputSuccess3" placeholder="Médico">
              <input type="text" class="form-control hidden" name="pacientes_id" id="inputSuccess3" value="">
              <input type="text" class="form-control hidden" name="fechaexp" id="inputSuccess3" value="<?php echo   date("Y") . "/" . date("m") . "/" .date("d"); ?>">
              <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
              <input type="date" name="fechaaten" class="form-control has-feedback-left" id="inputSuccess4" placeholder="Fecha de la Cita y Hora">
              <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
            </div>
            
            <div class='col-sm-4'>
                
                <div class="form-group">
                    <div class='input-group date' id='myDatepicker3'>
                        <input type='text' class="form-control" name="horatenc"/>
                        <span class="input-group-addon">
                           <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            
           
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                 <button class="btn btn-primary" type="reset" >Limpiar</button>
                <button type="submit" class="btn btn-success">Guardar</button>
              </div>
            </div>

          </form>
        </div>
      </div>
      

         

@endsection
