<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/cita', [App\Http\Controllers\AtencionController::class, 'cita'])->name('cita');
Route::get('/historialMedico', [App\Http\Controllers\AtencionController::class, 'historialMedico'])->name('historialMedico');
Route::get('/consultaMedico', [App\Http\Controllers\AtencionController::class, 'consultaMedico'])->name('consultaMedico');

Route::get('/informeMedico', [App\Http\Controllers\VistasController::class, 'informeMedico'])->name('informeMedico');
Route::get('/recetaMedica', [App\Http\Controllers\VistasController::class, 'recetaMedica'])->name('recetaMedica');
Route::get('/listaCitas', [App\Http\Controllers\VistasController::class, 'listaCitas'])->name('listaCitas');

Route::get('/paciente', [App\Http\Controllers\RegistroController::class, 'paciente'])->name('paciente');
Route::get('/medico', [App\Http\Controllers\RegistroController::class, 'medico'])->name('medico');
Route::get('/empleado', [App\Http\Controllers\RegistroController::class, 'empleado'])->name('empleado');
Route::get('/acompaniante', [App\Http\Controllers\RegistroController::class, 'acompaniante'])->name('acompaniante');

Route::get('/reporte', [App\Http\Controllers\EstadisticasController::class, 'reporte'])->name('reporte');
Route::get('/estadisticas', [App\Http\Controllers\EstadisticasController::class, 'estadisticas'])->name('estadisticas');


Route::get('/basico', [App\Http\Controllers\ConfiguracionController::class, 'basico'])->name('basico');
Route::get('/avanzado', [App\Http\Controllers\ConfiguracionController::class, 'avanzado'])->name('avanzado');
