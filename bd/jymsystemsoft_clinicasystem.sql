-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 27-02-2021 a las 04:11:52
-- Versión del servidor: 10.2.34-MariaDB
-- Versión de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `jymsystemsoft_clinicasystem`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acompañantes`
--

CREATE TABLE `acompañantes` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `dni` char(11) NOT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `direccion` varchar(200) DEFAULT NULL,
  `fecnac` date DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `generos_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `parentezco` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alergias`
--

CREATE TABLE `alergias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `alergias`
--

INSERT INTO `alergias` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(17, 'Amoxicilina', '2019-10-01 13:53:22', '2019-10-01 13:53:22'),
(18, 'Cefalexina', '2019-10-01 13:53:37', '2019-10-01 13:53:37'),
(19, 'Penicilina', '2019-10-01 13:54:18', '2019-10-01 13:54:18'),
(20, 'Ampilicina', '2019-10-01 13:57:02', '2019-10-01 13:57:02'),
(21, 'Dicloxacilina', '2019-10-01 13:57:20', '2019-10-01 13:57:20'),
(22, 'Nafcilina', '2019-10-01 13:57:37', '2019-10-01 13:57:37'),
(23, 'Oxacilina', '2019-10-01 13:58:44', '2019-10-01 13:58:44'),
(24, 'Penicilina G', '2019-10-01 13:59:11', '2019-10-01 13:59:11'),
(25, 'Penicilina V', '2019-10-01 13:59:21', '2019-10-01 13:59:21'),
(26, 'Piperacilina', '2019-10-01 13:59:42', '2019-10-01 13:59:42'),
(27, 'Ticarcilina', '2019-10-01 14:00:03', '2019-10-01 14:00:03'),
(28, 'Cefaclor', '2019-10-01 14:00:31', '2019-10-01 14:00:31'),
(29, 'Cefadroxilo', '2019-10-01 14:00:59', '2019-10-01 14:00:59'),
(30, 'Cefazolina', '2019-10-01 14:01:10', '2019-10-01 14:01:10'),
(31, 'Cefdinir', '2019-10-01 14:01:30', '2019-10-01 14:01:30'),
(32, 'Cefotetán', '2019-10-01 14:01:42', '2019-10-01 14:01:42'),
(33, 'Cefprozil', '2019-10-01 14:01:56', '2019-10-01 14:01:56'),
(34, 'Cefuroxima', '2019-10-01 14:02:14', '2019-10-01 14:02:14'),
(35, 'Cefalexina(Keflex)', '2019-10-01 14:02:24', '2019-10-01 14:02:44'),
(36, 'Cefepima (Maxipime)', '2019-10-01 14:03:08', '2019-10-01 14:03:08'),
(37, 'tramadol', '2019-11-27 20:55:56', '2019-11-27 20:55:56'),
(38, 'dfgdf', '2021-02-21 22:36:02', '2021-02-21 22:36:02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alergia_triaje`
--

CREATE TABLE `alergia_triaje` (
  `id` int(11) NOT NULL,
  `alergias_id` int(11) DEFAULT NULL,
  `triaje_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `antecpatologicos`
--

CREATE TABLE `antecpatologicos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(450) DEFAULT NULL,
  `triaje_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `antecpersonals`
--

CREATE TABLE `antecpersonals` (
  `id` int(11) NOT NULL,
  `fecultimregla` date DEFAULT NULL,
  `cangestaciones` int(11) DEFAULT NULL,
  `papanicolao` varchar(45) DEFAULT NULL,
  `ps_id` int(11) NOT NULL,
  `manticonceotivos_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `apetitos`
--

CREATE TABLE `apetitos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `apetitos`
--

INSERT INTO `apetitos` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'Bajo', NULL, NULL),
(2, 'Normal', NULL, NULL),
(3, 'Alto', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cies`
--

CREATE TABLE `cies` (
  `id` int(11) NOT NULL,
  `codigo` char(4) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tipocies_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cies`
--

INSERT INTO `cies` (`id`, `codigo`, `descripcion`, `created_at`, `updated_at`, `tipocies_id`) VALUES
(1, 'A000', 'COLERA DEBIDO A VIBRIO CHOLERAE O1, BIOTIPO CHOLERAE', NULL, NULL, 1),
(2, 'A001', 'COLERA DEBIDO A VIBRIO CHOLERAE O1, BIOTIPO EL TOR', NULL, NULL, 1),
(3, 'A009', 'COLERA NO ESPECIFICADO', NULL, NULL, 1),
(42, 'A010', 'FIEBRE TIFOIDEA', NULL, NULL, 1),
(43, 'A011', 'FIEBRE PARATIFOIDEA A', NULL, NULL, 1),
(44, 'A012', 'FIEBRE PARATIFOIDEA B', NULL, NULL, 1),
(45, 'A013', 'FIEBRE PARATIFOIDEA C', NULL, NULL, 1),
(46, 'A014', 'FIEBRE PARATIFOIDEA, NO ESPECIFICADA', NULL, NULL, 1),
(47, 'A020', 'ENTERITIS DEBIDA A SALMONELLA', NULL, NULL, 1),
(48, 'A021', 'SEPTICEMIA DEBIDA A SALMONELLA', NULL, NULL, 1),
(49, 'A022', 'INFECCIONES LOCALIZADAS DEBIDA A SALMONELLA', NULL, NULL, 1),
(50, 'A028', 'OTRAS INFECCIONES ESPECIFICADAS COMO DEBIDAS A SALMONELLA', NULL, NULL, 1),
(51, 'A029', 'INFECCIÓN DEBIDA A SALMONELLA NO ESPECIFICADA', NULL, NULL, 1),
(52, 'A030', 'SHIGELOSIS DEBIDA A SHIGELLA DYSENTERIAE', NULL, NULL, 1),
(53, 'A031', 'SHIGELOSIS DEBIDA A SHIGELLA FLEXNERI', NULL, NULL, 1),
(54, 'A032', 'SHIGELOSIS DEBIDA A SHIGELLA BOYDII', NULL, NULL, 1),
(55, 'A033', 'SHIGELOSIS DEBIDA A SHIGELLA SONNEI', NULL, NULL, 1),
(56, 'A038', 'OTRAS SHIGELOSIS', NULL, NULL, 1),
(57, 'A039', 'SHIGELOSIS DE TIPO NO ESPECIFICADO', NULL, NULL, 1),
(58, 'A040', 'INFECCION DEBIDA A ESCHERICHIA COLI ENTEROPATOGENA', NULL, NULL, 1),
(59, 'A041', 'INFECCION DEBIDA A ESCHERICHIA COLI ENTEROTOXIGENA', NULL, NULL, 1),
(60, 'A042', 'INFECCION DEBIDA A ESCHERICHIA COLI ENTEROINVASIVA', NULL, NULL, 1),
(61, 'A043', 'INFECCION DEBIDA A ESCHERICHIA COLI ENTEROHEMORRAGICA', NULL, NULL, 1),
(62, 'A044', 'OTRAS INFECCIONES INTESTINALES DEBIDAS A ESCHERICHIA COLI', NULL, NULL, 1),
(63, 'A045', 'ENTERITIS DEBIDA A CAMPYLOBACTER', NULL, NULL, 1),
(64, 'A046', 'ENTERITIS DEBIDA A YERSINIA ENTEROCOLITICA', NULL, NULL, 1),
(65, 'A047', 'ENTEROCOLITIS DEBIDA A CLOSTRIDIUM DIFFICILE', NULL, NULL, 1),
(66, 'A048', 'OTRAS INFECCIONES INTESTINALES BACTERIANAS ESPECIFICADAS', NULL, NULL, 1),
(67, 'A049', 'INFECCION INTESTINAL BACTERIANA, NO ESPECIFICADA', NULL, NULL, 1),
(68, 'A050', 'INTOXICACION ALIMENTARIA ESTAFILOCOCICA', NULL, NULL, 1),
(69, 'A051', 'BOTULISMO', NULL, NULL, 1),
(70, 'A052', 'INTOXICACION ALIMENTARIA DEBIDA A CLOSTRIDIUM PERFRINGENS [CLOSTRIDIUM WELCHII]', NULL, NULL, 1),
(71, 'A053', 'INTOXICACION ALIMENTARIA DEBIDA A VIBRIO PARAHAEMOLYTICUS', NULL, NULL, 1),
(72, 'A054', 'INTOXICACION ALIMENTARIA DEBIDA A BACILLUS CEREUS', NULL, NULL, 1),
(73, 'A058', 'OTRAS INTOXICACIONES ALIMENTARIAS DEBIDAS A BACTERIAS ESPECIFICADAS', NULL, NULL, 1),
(74, 'A059', 'INTOXICACION ALIMENTARIA BACTERIANA, NO ESPECIFICADA', NULL, NULL, 1),
(75, 'A060', 'DISENTERIA AMEBIANA AGUDA', NULL, NULL, 1),
(76, 'A061', 'AMEBIASIS INTESTINAL CRONICA', NULL, NULL, 1),
(77, 'A062', 'COLITIS AMEBIANA NO DISENTERICA', NULL, NULL, 1),
(78, 'A063', 'AMEBOMA INTESTINAL', NULL, NULL, 1),
(79, 'A064', 'ABSCESO AMEBIANO DEL HIGADO', NULL, NULL, 1),
(80, 'A065', 'ABSCESO AMEBIANO DEL PULMON (J99.8*)', NULL, NULL, 1),
(81, 'A066', 'ABSCESO AMEBIANO DEL CEREBRO (G07*)', NULL, NULL, 1),
(82, 'A067', 'AMEBIASIS CUTANEA', NULL, NULL, 1),
(83, 'A068', 'INFECCION AMEBIANA DE OTRAS LOCALIZACIONES', NULL, NULL, 1),
(84, 'A069', 'AMEBIASIS, NO ESPECIFICADA', NULL, NULL, 1),
(85, 'A070', 'BALANTIDIASIS', NULL, NULL, 1),
(86, 'A071', 'GIARDIASIS [LAMBLIASIS]', NULL, NULL, 1),
(87, 'A072', 'CRIPTOSPORIDIOSIS', NULL, NULL, 1),
(88, 'A073', 'ISOSPORIASIS', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `citas`
--

CREATE TABLE `citas` (
  `id` int(11) NOT NULL,
  `fecexp` date NOT NULL,
  `fecaten` date NOT NULL,
  `estado` varchar(45) NOT NULL,
  `motivo` text DEFAULT NULL,
  `tiempo` text NOT NULL,
  `horaten` varchar(45) NOT NULL,
  `pacientes_id` int(11) NOT NULL,
  `comprobante_id` int(11) DEFAULT NULL,
  `acompañantes_id` int(11) DEFAULT NULL,
  `users_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cita_exaginecologicos`
--

CREATE TABLE `cita_exaginecologicos` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `exaginecologicos_id` int(11) NOT NULL,
  `citas_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cita_motivo`
--

CREATE TABLE `cita_motivo` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `citas_id` int(11) NOT NULL,
  `tipoexaminters_id` int(11) NOT NULL,
  `documento` varchar(450) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comprobantes`
--

CREATE TABLE `comprobantes` (
  `id` int(11) NOT NULL,
  `fecexp` date NOT NULL,
  `serie` varchar(45) NOT NULL,
  `estado` varchar(45) NOT NULL,
  `monto` decimal(10,0) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `connsulta_exaginecologico`
--

CREATE TABLE `connsulta_exaginecologico` (
  `id` int(11) NOT NULL,
  `consultas_id` int(11) NOT NULL,
  `exaginecologicos_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consultas`
--

CREATE TABLE `consultas` (
  `id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `estado` varchar(45) NOT NULL,
  `citas_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `funcbiologicas_id` int(11) DEFAULT NULL,
  `motivo` varchar(450) DEFAULT NULL,
  `tiempo` varchar(45) DEFAULT NULL,
  `exaginecologicos_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consulta_diagnostico`
--

CREATE TABLE `consulta_diagnostico` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `consultas_id` int(11) NOT NULL,
  `diagnosticos_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deposicions`
--

CREATE TABLE `deposicions` (
  `id` int(11) NOT NULL,
  `nombre` varchar(450) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `deposicions`
--

INSERT INTO `deposicions` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'Bajo', NULL, NULL),
(2, 'Normal', NULL, NULL),
(3, 'Alto', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diagnostico_cies`
--

CREATE TABLE `diagnostico_cies` (
  `id` int(11) NOT NULL,
  `cies_id` int(11) NOT NULL,
  `consultas_id` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentos`
--

CREATE TABLE `documentos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `ruta` varchar(250) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `apellido` varchar(100) DEFAULT NULL,
  `dni` char(8) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tipoempleados_id` int(11) NOT NULL,
  `generos_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`id`, `nombre`, `apellido`, `dni`, `telefono`, `direccion`, `created_at`, `updated_at`, `tipoempleados_id`, `generos_id`) VALUES
(1, 'Pedro F.', 'Diaz Siccha', '72690062', '944646619', 'Jr. Magisterio', NULL, NULL, 1, 1),
(4, 'Andres', 'Ramirez Ochoa', '72658486', '946857895', 'Huaraz', '2019-08-29 06:38:00', '2019-08-29 06:38:00', 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadocivils`
--

CREATE TABLE `estadocivils` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `estadocivils`
--

INSERT INTO `estadocivils` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(3, 'Soltero', '2019-08-16 08:55:33', '2019-08-30 07:39:45'),
(4, 'Casado', '2019-08-22 03:26:45', '2019-08-30 07:38:11'),
(5, 'Casada', '2019-10-23 21:55:22', '2019-10-23 21:55:22'),
(6, 'Conviviente', '2020-02-05 12:16:30', '2020-02-05 12:16:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `exaexterno_ordenexam`
--

CREATE TABLE `exaexterno_ordenexam` (
  `id` int(11) NOT NULL,
  `detalle` varchar(450) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ordenexam_id` int(11) NOT NULL,
  `examenexterno_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `exaginecologicos`
--

CREATE TABLE `exaginecologicos` (
  `id` int(11) NOT NULL,
  `geybus` text NOT NULL,
  `cervix` text NOT NULL,
  `ovarios` text DEFAULT NULL,
  `vagina` text DEFAULT NULL,
  `utero` text DEFAULT NULL,
  `fondoseco` text DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examenexterno`
--

CREATE TABLE `examenexterno` (
  `id` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tipoexamexterno_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `examenexterno`
--

INSERT INTO `examenexterno` (`id`, `nombre`, `created_at`, `updated_at`, `tipoexamexterno_id`) VALUES
(1, 'Acido Úrico', NULL, NULL, 1),
(2, 'Amilasa Sérica', NULL, NULL, 1),
(3, 'Lipasa Sérica', NULL, NULL, 1),
(4, 'Bilirrubina Total y Fraccionada', NULL, NULL, 1),
(5, 'Colesterol HDL', NULL, NULL, 1),
(6, 'Grupo Sanguineo y Factor RH', NULL, NULL, 2),
(7, 'Hb', NULL, NULL, 2),
(8, 'Hto', NULL, NULL, 2),
(9, 'Hemograma Completo', NULL, NULL, 2),
(10, 'Recuento de Plaquetas', NULL, NULL, 2),
(11, 'Beta HCG Cualitativo', NULL, NULL, 3),
(12, 'Beta HCG Cuatitativo', NULL, NULL, 3),
(13, 'HIV', NULL, NULL, 3),
(14, 'RPR', NULL, NULL, 3),
(15, 'Test Elisa', NULL, NULL, 3),
(16, 'Estradiol', NULL, NULL, 4),
(17, 'Progeterona', NULL, NULL, 4),
(18, 'Prolactina', NULL, NULL, 4),
(19, 'F.S.H', NULL, NULL, 4),
(20, 'L.H.', NULL, NULL, 4),
(21, 'Alfa Feto-Proteina (AFP)', NULL, NULL, 5),
(22, 'Antígeno Carnico Embriogenico', NULL, NULL, 5),
(23, 'CA-125(ovario)', NULL, NULL, 5),
(24, 'CA15-3(mama)', NULL, NULL, 5),
(25, 'Colesterol LDL', NULL, NULL, 1),
(26, 'Colesterol', NULL, NULL, 1),
(27, 'Trigliceridos', NULL, NULL, 1),
(28, 'Deshidrogenosa Láctica (DHL)', NULL, NULL, 1),
(29, 'Creatinina Sérica', NULL, NULL, 1),
(30, 'Glucosa Basal', NULL, NULL, 1),
(31, 'Hb Glicosilada', NULL, NULL, 1),
(32, 'Tolerancia de Glucosa x 5 Deter', NULL, NULL, 1),
(33, 'Proteínas Totales y Fraccionadas', NULL, NULL, 1),
(34, 'TGO', NULL, NULL, 1),
(35, 'TGP', NULL, NULL, 1),
(36, 'Exámen de Orina Completo', NULL, NULL, 1),
(37, 'Urocultibo - Antib. (12 colonias)', NULL, NULL, 1),
(38, 'Proteína en Orina de 24 Hrs', NULL, NULL, 1),
(39, 'Hisopado de secr. Vag. - Graham', NULL, NULL, 1),
(40, 'Test de Helechos', NULL, NULL, 1),
(41, 'Gota Gruesa de Paludismo', NULL, NULL, 2),
(42, 'Tiempo de Protrombina', NULL, NULL, 2),
(43, 'Tiempo Parcial de Tromboplastina', NULL, NULL, 2),
(44, 'Veloc. de Sedimentación', NULL, NULL, 2),
(45, 'Chlamydia Trachomatis IGM', NULL, NULL, 3),
(46, 'Chlamydia Trachomatis IGG', NULL, NULL, 3),
(47, 'Citomegalovirus IGM', NULL, NULL, 3),
(48, 'Toxoplasma IGM', NULL, NULL, 3),
(49, 'Hepatitis A IGM', NULL, NULL, 3),
(50, 'Hepatitis B Cualitativo', NULL, NULL, 3),
(51, 'Hepatitis B Core IGM', NULL, NULL, 3),
(52, 'Herpes I - IGM', NULL, NULL, 3),
(53, 'Herpes I - IGG', NULL, NULL, 3),
(54, 'Herpes II - IGM', NULL, NULL, 3),
(55, 'Herpes II - IGG', NULL, NULL, 3),
(56, 'Hidatidosis (Western Blot)', NULL, NULL, 3),
(57, 'Rubeola IGM', NULL, NULL, 3),
(58, 'Varicela IGM', NULL, NULL, 3),
(59, 'Helicobacter Pylori Cualitativo', NULL, NULL, 3),
(60, 'Proteina C Reactiva - Latex', NULL, NULL, 3),
(61, 'Test de Coombs Indirecto', NULL, NULL, 3),
(62, 'Aglutinaciones', NULL, NULL, 3),
(63, 'T3 (Tiroxina)', NULL, NULL, 4),
(64, 'T4 (Triiodotironina)', NULL, NULL, 4),
(65, 'TSH Ultrasensible', NULL, NULL, 4),
(66, 'DH - epi - Androstenediona', NULL, NULL, 4),
(67, 'CA19 - 9 (Páncreas)', NULL, NULL, 5),
(68, 'CA - 549 (Mama)', NULL, NULL, 5),
(69, 'CA72 - 4 (Estómago)', NULL, NULL, 5),
(70, 'Antig. Prostático Específico', NULL, NULL, 5),
(72, 'Espermatograma', NULL, NULL, 6),
(73, 'Oxiurus (Test de Grham - Parche)', NULL, NULL, 7),
(74, 'Exámen de Thevenon', NULL, NULL, 7),
(75, 'Colesterol Total', NULL, NULL, 8),
(76, 'Triglicéridos', NULL, NULL, 8),
(77, 'HDL/LDL', NULL, NULL, 8),
(78, 'VLDL', NULL, NULL, 8),
(79, 'Lípidos Totales', NULL, NULL, 8),
(80, 'Bilirrubina T y F', NULL, NULL, 9),
(81, 'TGO - TGP', NULL, NULL, 9),
(82, 'Fosfatasa Alcalina', NULL, NULL, 9),
(83, 'Gamaglutamil', NULL, NULL, 9),
(84, 'Proteínas T y F', NULL, NULL, 9),
(85, 'Ácido Úrico', NULL, NULL, 10),
(86, 'Factor Reumatoideo', NULL, NULL, 10),
(87, 'VSG', NULL, NULL, 10),
(88, 'ASO', NULL, NULL, 10),
(89, 'PCR', NULL, NULL, 10),
(90, 'Grupo Sanguíneo/RH', NULL, NULL, 11),
(91, 'Hemograma C. - Recuento P.', NULL, NULL, 11),
(92, 'Colesterol T', NULL, NULL, 11),
(93, 'Trigliceridos', NULL, NULL, 11),
(94, 'Glucosa Basa', NULL, NULL, 11),
(95, 'Tiempor de Protrombina', NULL, NULL, 11),
(96, 'Grupo Sanguíneo / RH', NULL, NULL, 12),
(97, 'Hemoglobina', NULL, NULL, 12),
(98, 'Hemotrocito', NULL, NULL, 12),
(99, 'Glucosa Basal', NULL, NULL, 12),
(100, 'Creatinina Sérica', NULL, NULL, 12),
(101, 'VIH', NULL, NULL, 12),
(102, 'RPR', NULL, NULL, 12),
(103, 'Antígeno Australiano', NULL, NULL, 12),
(104, 'Exámen de Orina Completo', NULL, NULL, 12),
(105, 'Grupo Sanguíneo / RH', NULL, NULL, 13),
(106, 'Hemograma Completo', NULL, NULL, 13),
(107, 'Recuento de Plaquetas', NULL, NULL, 13),
(108, 'Tiempo de Protrombina', NULL, NULL, 13),
(109, 'Glucosa Basal', NULL, NULL, 13),
(110, 'Creatinina Sérica', NULL, NULL, 13),
(111, 'RPR/VIH', NULL, NULL, 13),
(112, 'Antígeno Australiano', NULL, NULL, 13),
(113, 'Exámen de Orina Completo', NULL, NULL, 13),
(114, 'T3', NULL, NULL, 14),
(115, 'T4', NULL, NULL, 14),
(116, 'TSH', NULL, NULL, 14),
(117, 'Hemograma Completo', NULL, NULL, 15),
(118, 'Glucosa Basal', NULL, NULL, 15),
(119, 'Exámen de Orina Completo', NULL, NULL, 15),
(120, 'Hemograma Completo', NULL, NULL, 16),
(121, 'Glucosa Basal', NULL, NULL, 16),
(122, 'Creatinina Sérica', NULL, NULL, 16),
(123, 'TGO', NULL, NULL, 16),
(124, 'Colesterol', NULL, NULL, 16),
(125, 'Exámen de Orina COmpleto', NULL, NULL, 16),
(126, 'Tiempo de Protrombina', NULL, NULL, 17),
(127, 'TIempo Parcial de Tromboplastina', NULL, NULL, 17),
(128, 'Recuento de Plaquetas', NULL, NULL, 17),
(129, 'Ecografía Abdomen Completo', NULL, NULL, 18),
(130, 'Ecografía Transvaginal', NULL, NULL, 18),
(131, 'Ecografía de Mamas', NULL, NULL, 18),
(132, 'Ecografía de Tiroidez', NULL, NULL, 18),
(133, 'Ecografía Obsetétrica 2D', NULL, NULL, 18),
(134, 'Ecpgrafía de Partes Blandas', NULL, NULL, 18),
(135, 'Ecografía Testicular', NULL, NULL, 18),
(136, 'Ecografía Protática y Renal', NULL, NULL, 18),
(137, 'Ecografía Obstétrica 3D y 4D', NULL, NULL, 18),
(138, 'Ecografía Doppler', NULL, NULL, 18),
(139, 'Exámen de Papanicolau', NULL, NULL, 19),
(140, 'Video Colposcopia', NULL, NULL, 19),
(265, 'Prueba de paternidad/maternidad por ADN', NULL, NULL, 20),
(266, 'ADN para Identificación', NULL, NULL, 20),
(267, 'Prueba de paternidad pre natal', NULL, NULL, 20),
(268, 'Perfil genético', NULL, NULL, 20),
(269, 'Cromosomas (careotipo) en sangre', NULL, NULL, 20),
(270, 'Cromosomas (careotipo) philadelphia', NULL, NULL, 20),
(271, 'Cromosomas (careotipo) en médula ósea', NULL, NULL, 20),
(272, 'Cromosomas (careotipo) en otros tejidos: fibroplastos, piel', NULL, NULL, 20),
(273, 'Cromatina sexual (Barr)', NULL, NULL, 20),
(274, 'Cromosomas (careotipo) en sangre fetal', NULL, NULL, 20),
(275, 'Cromosomas (careotipo) de pareja con aborto recurrente', NULL, NULL, 20),
(276, 'Cromosomas (careotipo) de restos embrionarios', NULL, NULL, 20),
(277, 'Cromosomas (careotipo) del natimuerto', NULL, NULL, 20),
(278, 'Cromosomas (careotipo) en restos embrionarios', NULL, NULL, 20),
(279, 'Tamiz Básico (5 enfermedades)', NULL, NULL, 20),
(280, 'Tamiz Ampliado (40 enfermedades)', NULL, NULL, 20),
(281, 'Metabolitos anormales en orina (Pruebas de 4 a 9)', NULL, NULL, 20),
(282, 'Ácido fenilpirúvico y deribados (PKU)', NULL, NULL, 20),
(283, 'Alfacetoácidos (orina de jarabe de arce)', NULL, NULL, 20),
(284, 'Cistina/homocistina', NULL, NULL, 20),
(285, 'Aminiaciduría renal (defecto transporte)', NULL, NULL, 20),
(286, 'Azucares reductores (glucosa galactosa otros)', NULL, NULL, 20),
(287, 'Acido homogenticico (alcaptonuria)', NULL, NULL, 20),
(288, 'Mucopolisacáridos)', NULL, NULL, 20),
(289, 'Espectrometría de masas en tandem (MS-MS) Aminoácidos y Acilcarnitinas)', NULL, NULL, 20),
(290, 'Cromosomas en sangre con caracteres neoplásicos)', NULL, NULL, 20),
(291, 'Cromosomas en médula ósea)', NULL, NULL, 20),
(292, 'Cromosomas philadelphia)', NULL, NULL, 20),
(293, 'Cromosomas en tejido tumoral)', NULL, NULL, 20),
(294, 'Asesoramiento genetico en cáncer familiar)', NULL, NULL, 20),
(295, 'Cancer de mama familiar (BRCA 1 - BRCA 2, OTROS)', NULL, NULL, 20),
(296, 'Cancer de colón', NULL, NULL, 20),
(297, 'Poliposis de colon familiar', NULL, NULL, 20),
(298, 'PPaneles de genes relacionados con cancer hereditario', NULL, NULL, 20),
(299, 'Cromosomas (cariotipo) en líquido amniótico', NULL, NULL, 20),
(300, 'Cromosomas (cariotipo) en bellosidades coriales', NULL, NULL, 20),
(301, 'Cromosomas (cariotipo) en sangre de cordón umbilical', NULL, NULL, 20),
(302, 'Fish en líquido amniótico: 13,18,21,X,Y', NULL, NULL, 20),
(303, 'microarray - Pre natal (líquido amniótico)', NULL, NULL, 20),
(304, 'Análisis Pre natal básico no invasivo sangre materna', NULL, NULL, 20),
(305, 'Análisis Pre natal plus no invasivo sangre materna', NULL, NULL, 20),
(306, 'Panel molecular (descarte de enfermedades hereditarias (300 genes) Parejas interesadas en evaluar el riesgo para su descendencia. Precio por persona)', NULL, NULL, 20),
(307, 'Ataxia Spinocerebral - Repetición de Tripletes', NULL, NULL, 20),
(308, 'Autismo (Panel)', NULL, NULL, 20),
(309, 'Sordera: Conexina 26 y otras  (Panel)', NULL, NULL, 20),
(310, 'Corea de Huntington', NULL, NULL, 20),
(311, 'CGH - Microarray 750-180K', NULL, NULL, 20),
(312, 'Distrofia miotónica', NULL, NULL, 20),
(313, 'Distrofia muscular Duchenne/Becker y otras', NULL, NULL, 20),
(314, 'Enfermedad poliquística renal', NULL, NULL, 20),
(315, 'Enfermedades mitocondriales - ADN Mitocondrial', NULL, NULL, 20),
(316, 'Epilepsia (Panel) 554 genes + ADN Mitocondrial', NULL, NULL, 20),
(317, 'Exoma clpinico (7000 genes)', NULL, NULL, 20),
(318, 'Exoma Trio', NULL, NULL, 20),
(319, 'Fibrosis quística', NULL, NULL, 20),
(320, 'Hibridación Genómica comparada (CGH), Array de cromosomas, microarray de 180k', NULL, NULL, 20),
(321, 'Polineoropatias (Panel)', NULL, NULL, 20),
(322, 'Retardo mental (Panel)', NULL, NULL, 20),
(323, 'S. Angelman', NULL, NULL, 20),
(324, 'S. Prader Willi', NULL, NULL, 20),
(325, 'S. Rett', NULL, NULL, 20),
(326, 'X-Frágil', NULL, NULL, 20);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `exauxresult`
--

CREATE TABLE `exauxresult` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `detalle` varchar(45) DEFAULT NULL,
  `ruta` varchar(450) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cita_motivo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `funcbiologicas`
--

CREATE TABLE `funcbiologicas` (
  `id` int(11) NOT NULL,
  `apetitos_id` int(11) NOT NULL,
  `deposicions_id` int(11) NOT NULL,
  `seds_id` int(11) NOT NULL,
  `sueños_id` int(11) NOT NULL,
  `orinas_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `funcvitals`
--

CREATE TABLE `funcvitals` (
  `id` int(11) NOT NULL,
  `presarterials_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pulso` varchar(45) DEFAULT NULL,
  `temperatura` varchar(45) DEFAULT NULL,
  `talla` varchar(45) DEFAULT NULL,
  `peso` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `generos`
--

CREATE TABLE `generos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `generos`
--

INSERT INTO `generos` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'Masculino', NULL, NULL),
(2, 'Femenino', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `insgenerals`
--

CREATE TABLE `insgenerals` (
  `id` int(11) NOT NULL,
  `nombre` varchar(450) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `insgenerals`
--

INSERT INTO `insgenerals` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'Cabeza', NULL, NULL),
(2, 'Cuello', NULL, NULL),
(3, 'Tórax', NULL, NULL),
(4, 'Mamas', NULL, NULL),
(5, 'Pulmones y Cardiovascular', NULL, NULL),
(6, 'Abdomen', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `insgeneral_consulta`
--

CREATE TABLE `insgeneral_consulta` (
  `id` int(11) NOT NULL,
  `resultado` varchar(450) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `insgenerals_id` int(11) NOT NULL,
  `consultas_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instruccions`
--

CREATE TABLE `instruccions` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `instruccions`
--

INSERT INTO `instruccions` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(3, 'Superior Universitaria', '2019-08-16 08:55:21', '2019-08-16 08:55:21'),
(4, 'Secundaria', '2019-08-22 03:26:15', '2019-08-22 03:26:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `laboratorios`
--

CREATE TABLE `laboratorios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `direccion` varchar(450) NOT NULL,
  `telefono` varchar(45) NOT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `ruc` char(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `laboratorios`
--

INSERT INTO `laboratorios` (`id`, `nombre`, `direccion`, `telefono`, `correo`, `ruc`, `created_at`, `updated_at`) VALUES
(2, 'Suiza Lab', 'Hz', '326589', 'suizaLab@gmail.com', '20358975894', NULL, NULL),
(3, 'Peru Lab', 'Hz', '457898', 'peru@hotmail,com', '20165478968', NULL, NULL),
(4, 'Lab1', 'Jr. Magusterio', '043-226658', 'lab1@gmail.com', '20354687945', '2019-08-29 10:33:05', '2019-08-29 10:33:05'),
(5, 'Otros', '', '', NULL, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `manticonceptivos`
--

CREATE TABLE `manticonceptivos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `manticonceptivos`
--

INSERT INTO `manticonceptivos` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'Pildora', NULL, NULL),
(2, 'SIU', NULL, NULL),
(3, 'Condones', NULL, NULL),
(4, 'Parche Anticonceptivo', NULL, NULL),
(5, 'Anillo', NULL, NULL),
(6, 'Implante', NULL, NULL),
(7, 'Inyeccion', NULL, NULL),
(8, 'DIU', NULL, NULL),
(9, 'Condon Femenino', NULL, NULL),
(10, 'Diafragma', NULL, NULL),
(11, 'Conciencia de la Fertilidad', NULL, NULL),
(12, 'Capuchon Cervical', NULL, NULL),
(13, 'Esponja', NULL, NULL),
(14, 'Espermicidas', NULL, NULL),
(17, 'Natural', '2019-09-06 15:22:34', '2019-09-06 16:19:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medicamentos`
--

CREATE TABLE `medicamentos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `concentracion` varchar(200) DEFAULT NULL,
  `presentacion` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `medicamentos`
--

INSERT INTO `medicamentos` (`id`, `nombre`, `created_at`, `updated_at`, `concentracion`, `presentacion`) VALUES
(1, 'Paracetamol', NULL, NULL, '500ml', 'Tableta'),
(2, 'Amoxicilina', NULL, '2019-09-27 15:15:04', '80g', 'Capsula'),
(7, 'Ibuprofeno', '2019-12-23 18:34:29', '2019-12-23 18:34:29', '20 ml', 'Jarabe'),
(8, 'Funcaden', '2020-03-09 21:33:01', '2020-03-09 21:33:01', '150 mg', 'Ovulos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `id` int(11) NOT NULL,
  `permissions_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `model_id` varchar(45) DEFAULT NULL,
  `model_type` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `observacions`
--

CREATE TABLE `observacions` (
  `id` int(11) NOT NULL,
  `nombre` varchar(450) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `consultas_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ocupacions`
--

CREATE TABLE `ocupacions` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ocupacions`
--

INSERT INTO `ocupacions` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(4, 'Estudiante', '2019-08-16 08:54:58', '2019-08-16 08:54:58'),
(5, 'Ama de Casa', '2019-08-22 03:25:20', '2019-08-22 03:25:20'),
(6, 'Ingeniera', '2019-12-12 19:51:13', '2019-12-12 19:51:13'),
(7, 'CONTADORA', '2020-02-05 12:15:58', '2020-02-05 12:15:58'),
(8, 'Administrador', '2020-03-09 21:01:22', '2020-03-09 21:01:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ordenexam`
--

CREATE TABLE `ordenexam` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(450) NOT NULL,
  `fecha` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `consultas_id` int(11) NOT NULL,
  `estado` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orinas`
--

CREATE TABLE `orinas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(450) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `orinas`
--

INSERT INTO `orinas` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'Bajo', NULL, NULL),
(2, 'Normal', NULL, NULL),
(3, 'Alto', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pacientes`
--

CREATE TABLE `pacientes` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `dni` char(11) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `lugarnac` varchar(100) DEFAULT NULL,
  `lugarproc` varchar(100) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `fecnac` date DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `ocupacions_id` int(11) NOT NULL,
  `instruccions_id` int(11) NOT NULL,
  `estadocivils_id` int(11) NOT NULL,
  `generos_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parentezcos`
--

CREATE TABLE `parentezcos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `acompañantes_id` int(11) NOT NULL,
  `pacientes_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `name` varchar(191) NOT NULL,
  `guard_name` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `presarterials`
--

CREATE TABLE `presarterials` (
  `id` int(11) NOT NULL,
  `sistolica` varchar(45) DEFAULT NULL,
  `diastolica` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ps`
--

CREATE TABLE `ps` (
  `id` int(11) NOT NULL,
  `valora` int(11) DEFAULT NULL,
  `valorb` int(11) DEFAULT NULL,
  `valorc` int(11) DEFAULT NULL,
  `valord` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resultadoexamen`
--

CREATE TABLE `resultadoexamen` (
  `id` int(11) NOT NULL,
  `nombre` varchar(450) DEFAULT NULL,
  `estado` varchar(45) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ruta` varchar(450) DEFAULT NULL,
  `descripcion` varchar(450) DEFAULT NULL,
  `laboratorios_id` int(11) NOT NULL,
  `ordenexam_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(191) NOT NULL,
  `guard_name` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seds`
--

CREATE TABLE `seds` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `seds`
--

INSERT INTO `seds` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'Disminuido', NULL, '2019-11-27 21:11:16'),
(2, 'Normal', NULL, NULL),
(3, 'Elevado', NULL, '2019-11-27 21:10:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sueños`
--

CREATE TABLE `sueños` (
  `id` int(11) NOT NULL,
  `nombre` varchar(450) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sueños`
--

INSERT INTO `sueños` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'Bajo', NULL, NULL),
(2, 'Normal', NULL, NULL),
(3, 'Alto', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipocies`
--

CREATE TABLE `tipocies` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipocies`
--

INSERT INTO `tipocies` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'Diagnostico', NULL, NULL),
(2, 'Procedimiento', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoempleados`
--

CREATE TABLE `tipoempleados` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipoempleados`
--

INSERT INTO `tipoempleados` (`id`, `nombre`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Activo', NULL, NULL),
(4, 'Medico', 'Activo', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoexamexterno`
--

CREATE TABLE `tipoexamexterno` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `detalle` varchar(450) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipoexamexterno`
--

INSERT INTO `tipoexamexterno` (`id`, `nombre`, `detalle`, `created_at`, `updated_at`) VALUES
(1, 'BIOQUIMICO', NULL, NULL, NULL),
(2, 'HEMATOLOGIA', NULL, NULL, NULL),
(3, 'INMUNOLOGIA', NULL, NULL, NULL),
(4, 'ENDOCRINOLOGIA', NULL, NULL, NULL),
(5, 'MARCADORES TUMORALES', NULL, NULL, NULL),
(6, 'MICROBIOLOGIA', NULL, NULL, NULL),
(7, 'PARASITOLOGIA', NULL, NULL, NULL),
(8, 'PERFIL LIPIDICO', NULL, NULL, NULL),
(9, 'PERFIL HEPATICO', NULL, NULL, NULL),
(10, 'PERFIL REUMATOLOGICO', NULL, NULL, NULL),
(11, 'PERFIL CORONARIO', NULL, NULL, NULL),
(12, 'PERFIL GESTANTE', NULL, NULL, NULL),
(13, 'PERFIL PREQUIRUUGICO', NULL, NULL, NULL),
(14, 'PERFIL TIROIDEO', NULL, NULL, NULL),
(15, 'PAQUETE ITU', NULL, NULL, NULL),
(16, 'CHEQUEO GENERAL', NULL, NULL, NULL),
(17, 'PERFIL DE COAGULACION', NULL, NULL, NULL),
(18, 'ECOGRAFIAS', NULL, NULL, NULL),
(19, 'EXÁMENES', NULL, NULL, NULL),
(20, 'GENÉTICA', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoexaminters`
--

CREATE TABLE `tipoexaminters` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipoexaminters`
--

INSERT INTO `tipoexaminters` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(4, 'Consulta', NULL, NULL),
(5, 'Eco. Abdominal', NULL, NULL),
(6, 'Eco. Transvaginal', NULL, NULL),
(7, 'Eco. de Mamas', NULL, NULL),
(8, 'Eco. Tiroidez', NULL, NULL),
(9, 'Eco. Obstetrica 2D', NULL, NULL),
(10, 'Eco. Partes Blandas', NULL, NULL),
(11, 'Eco. Testicular', NULL, NULL),
(12, 'Eco. Renal y Prostatica', NULL, NULL),
(13, 'Eco. Obstetrica 3D-4D', NULL, NULL),
(14, 'Eco. Doppler', NULL, NULL),
(15, 'Chequeo Ginecológico', NULL, NULL),
(16, 'Exámen Citológico', NULL, NULL),
(17, 'Vidéo Coloscopía', NULL, NULL),
(18, 'Biopsia', NULL, NULL),
(19, 'VPH', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tratamientos`
--

CREATE TABLE `tratamientos` (
  `id` int(11) NOT NULL,
  `indicacion` varchar(450) DEFAULT NULL,
  `consultas_id` int(11) NOT NULL,
  `medicamentos_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cantidad` varchar(450) DEFAULT NULL,
  `dosis` varchar(450) DEFAULT NULL,
  `via` varchar(450) DEFAULT NULL,
  `frecuencia` varchar(450) DEFAULT NULL,
  `duracion` varchar(450) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `triaje`
--

CREATE TABLE `triaje` (
  `id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `antecpersonals_id` int(11) NOT NULL,
  `citas_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `funcvitals_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `triaje`
--

INSERT INTO `triaje` (`id`, `fecha`, `antecpersonals_id`, `citas_id`, `users_id`, `created_at`, `updated_at`, `funcvitals_id`) VALUES
(62, '2019-12-19', 72, 64, 3, '2019-12-19 20:09:18', '2019-12-19 20:09:18', 85),
(63, '2019-12-19', 73, 64, 3, '2019-12-19 20:09:18', '2019-12-19 20:09:18', 86),
(64, '2019-12-19', 74, 64, 3, '2019-12-19 20:09:18', '2019-12-19 20:09:18', 87),
(65, '2019-12-19', 75, 64, 3, '2019-12-19 20:09:18', '2019-12-19 20:09:18', 88),
(66, '2019-12-19', 76, 64, 3, '2019-12-19 20:09:18', '2019-12-19 20:09:18', 89),
(67, '2019-12-19', 77, 64, 3, '2019-12-19 20:09:19', '2019-12-19 20:09:19', 90),
(68, '2019-12-19', 78, 64, 3, '2019-12-19 20:09:19', '2019-12-19 20:09:19', 91),
(69, '2019-12-19', 79, 64, 3, '2019-12-19 20:09:19', '2019-12-19 20:09:19', 92),
(70, '2019-12-19', 80, 64, 3, '2019-12-19 20:09:19', '2019-12-19 20:09:19', 93),
(71, '2019-12-19', 81, 64, 3, '2019-12-19 20:09:19', '2019-12-19 20:09:19', 94),
(72, '2019-12-19', 82, 64, 3, '2019-12-19 20:09:28', '2019-12-19 20:09:28', 95),
(73, '2019-12-19', 83, 64, 3, '2019-12-19 20:09:31', '2019-12-19 20:09:31', 96),
(74, '2019-12-19', 84, 64, 3, '2019-12-19 20:12:07', '2019-12-19 20:12:07', 97),
(75, '2019-12-19', 85, 64, 3, '2019-12-19 20:13:01', '2019-12-19 20:13:01', 98),
(76, '2019-12-23', 86, 60, 3, '2019-12-23 16:02:24', '2019-12-23 16:02:24', 99),
(77, '2019-12-23', 87, 60, 3, '2019-12-23 16:03:31', '2019-12-23 16:03:31', 100),
(78, '2019-12-23', 88, 59, 3, '2019-12-23 16:14:21', '2019-12-23 16:14:21', 101),
(79, '2019-12-23', 89, 59, 3, '2019-12-23 16:14:35', '2019-12-23 16:14:35', 102),
(80, '2019-12-23', 90, 59, 3, '2019-12-23 16:14:54', '2019-12-23 16:14:54', 103),
(81, '2019-12-23', 91, 65, 3, '2019-12-23 16:27:21', '2019-12-23 16:27:21', 104),
(82, '2019-12-23', 92, 65, 3, '2019-12-23 16:28:48', '2019-12-23 16:28:48', 105),
(83, '2019-12-23', 93, 65, 3, '2019-12-23 16:30:56', '2019-12-23 16:30:56', 106),
(84, '2019-12-23', 94, 65, 3, '2019-12-23 17:01:34', '2019-12-23 17:01:34', 107),
(85, '2019-12-23', 95, 65, 3, '2019-12-23 17:01:41', '2019-12-23 17:01:41', 108),
(86, '2019-12-23', 96, 65, 3, '2019-12-23 17:02:39', '2019-12-23 17:02:39', 109),
(87, '2019-12-23', 97, 65, 3, '2019-12-23 17:02:43', '2019-12-23 17:02:43', 110),
(88, '2019-12-23', 98, 66, 3, '2019-12-23 17:13:47', '2019-12-23 17:13:47', 111),
(89, '2019-12-23', 99, 66, 3, '2019-12-23 17:14:12', '2019-12-23 17:14:12', 112),
(90, '2019-12-23', 100, 66, 3, '2019-12-23 17:14:19', '2019-12-23 17:14:19', 113),
(91, '2019-12-23', 101, 66, 3, '2019-12-23 17:14:32', '2019-12-23 17:14:32', 114),
(92, '2019-12-23', 102, 66, 3, '2019-12-23 17:14:58', '2019-12-23 17:14:58', 115),
(93, '2019-12-23', 103, 66, 3, '2019-12-23 17:15:21', '2019-12-23 17:15:21', 116),
(94, '2019-12-23', 104, 66, 3, '2019-12-23 17:15:38', '2019-12-23 17:15:38', 117),
(95, '2019-12-23', 105, 65, 3, '2019-12-23 17:15:46', '2019-12-23 17:15:46', 118),
(96, '2019-12-23', 106, 66, 3, '2019-12-23 17:15:57', '2019-12-23 17:15:57', 119),
(97, '2019-12-23', 107, 66, 3, '2019-12-23 17:16:02', '2019-12-23 17:16:02', 120),
(98, '2019-12-23', 108, 66, 3, '2019-12-23 17:16:05', '2019-12-23 17:16:05', 121),
(99, '2019-12-23', 109, 66, 3, '2019-12-23 17:16:19', '2019-12-23 17:16:19', 122),
(100, '2019-12-23', 110, 65, 3, '2019-12-23 17:16:59', '2019-12-23 17:16:59', 123),
(101, '2019-12-23', 111, 65, 3, '2019-12-23 17:17:03', '2019-12-23 17:17:03', 124),
(102, '2019-12-23', 112, 65, 3, '2019-12-23 17:17:16', '2019-12-23 17:17:16', 125),
(103, '2019-12-23', 113, 65, 3, '2019-12-23 17:17:18', '2019-12-23 17:17:18', 126),
(104, '2019-12-23', 114, 65, 3, '2019-12-23 17:17:23', '2019-12-23 17:17:23', 127),
(105, '2019-12-23', 115, 65, 3, '2019-12-23 17:17:37', '2019-12-23 17:17:37', 128),
(106, '2019-12-23', 116, 67, 3, '2019-12-23 18:19:09', '2019-12-23 18:19:09', 129),
(107, '2019-12-23', 117, 67, 3, '2019-12-23 18:19:21', '2019-12-23 18:19:21', 130),
(108, '2019-12-23', 118, 67, 3, '2019-12-23 18:19:28', '2019-12-23 18:19:28', 131),
(109, '2019-12-23', 119, 67, 3, '2019-12-23 18:19:58', '2019-12-23 18:19:58', 132),
(110, '2019-12-23', 120, 67, 3, '2019-12-23 18:20:02', '2019-12-23 18:20:02', 133),
(111, '2019-12-23', 121, 67, 3, '2019-12-23 18:20:07', '2019-12-23 18:20:07', 134),
(112, '2019-12-23', 122, 67, 3, '2019-12-23 18:20:23', '2019-12-23 18:20:23', 135),
(113, '2019-12-23', 123, 64, 3, '2019-12-23 19:04:13', '2019-12-23 19:04:13', 136),
(114, '2019-12-23', 124, 64, 3, '2019-12-23 19:04:18', '2019-12-23 19:04:18', 137),
(115, '2019-12-23', 125, 64, 3, '2019-12-23 19:04:22', '2019-12-23 19:04:22', 138),
(116, '2019-12-23', 126, 64, 3, '2019-12-23 19:04:42', '2019-12-23 19:04:42', 139),
(117, '2020-01-03', 127, 68, 3, '2020-01-03 00:10:01', '2020-01-03 00:10:01', 140),
(118, '2020-01-03', 128, 68, 3, '2020-01-03 00:10:05', '2020-01-03 00:10:05', 141),
(119, '2020-01-03', 129, 68, 3, '2020-01-03 00:10:12', '2020-01-03 00:10:12', 142),
(120, '2020-01-03', 130, 68, 3, '2020-01-03 00:10:23', '2020-01-03 00:10:23', 143),
(121, '2020-01-03', 131, 70, 3, '2020-01-03 00:11:12', '2020-01-03 00:11:12', 144),
(122, '2020-01-03', 132, 70, 3, '2020-01-03 00:11:15', '2020-01-03 00:11:15', 145),
(123, '2020-01-03', 133, 70, 3, '2020-01-03 00:11:19', '2020-01-03 00:11:19', 146),
(124, '2020-01-03', 134, 70, 3, '2020-01-03 00:11:35', '2020-01-03 00:11:35', 147),
(125, '2020-01-03', 135, 69, 3, '2020-01-03 00:12:45', '2020-01-03 00:12:45', 148),
(126, '2020-01-03', 136, 69, 3, '2020-01-03 00:12:50', '2020-01-03 00:12:50', 149),
(127, '2020-01-03', 137, 69, 3, '2020-01-03 00:12:55', '2020-01-03 00:12:55', 150),
(128, '2020-01-03', 138, 69, 3, '2020-01-03 00:13:09', '2020-01-03 00:13:09', 151),
(129, '2020-02-05', 139, 71, 3, '2020-02-05 12:19:19', '2020-02-05 12:19:19', 152),
(130, '2020-02-05', 140, 71, 3, '2020-02-05 12:19:52', '2020-02-05 12:19:52', 153),
(131, '2020-02-05', 141, 71, 3, '2020-02-05 12:20:33', '2020-02-05 12:20:33', 154),
(132, '2020-02-05', 142, 71, 3, '2020-02-05 12:20:44', '2020-02-05 12:20:44', 155),
(133, '2020-02-05', 143, 71, 3, '2020-02-05 12:21:32', '2020-02-05 12:21:32', 156),
(134, '2020-02-05', 144, 71, 3, '2020-02-05 12:21:51', '2020-02-05 12:21:51', 157),
(135, '2020-02-05', 145, 71, 3, '2020-02-05 12:22:39', '2020-02-05 12:22:39', 158),
(136, '2020-02-25', 146, 72, 3, '2020-02-25 19:44:35', '2020-02-25 19:44:35', 159),
(137, '2020-02-25', 147, 72, 3, '2020-02-25 19:45:48', '2020-02-25 19:45:48', 160),
(138, '2020-02-25', 148, 72, 3, '2020-02-25 19:46:45', '2020-02-25 19:46:45', 161),
(139, '2020-02-25', 149, 72, 3, '2020-02-25 19:48:10', '2020-02-25 19:48:10', 162),
(140, '2020-02-25', 150, 72, 3, '2020-02-25 19:50:37', '2020-02-25 19:50:37', 163),
(141, '2020-02-25', 151, 72, 3, '2020-02-25 19:50:41', '2020-02-25 19:50:41', 164),
(142, '2020-02-25', 152, 72, 3, '2020-02-25 19:50:48', '2020-02-25 19:50:48', 165),
(143, '2020-02-25', 153, 72, 3, '2020-02-25 19:50:51', '2020-02-25 19:50:51', 166),
(144, '2020-02-29', 154, 74, 3, '2020-02-29 22:59:36', '2020-02-29 22:59:36', 167),
(145, '2020-02-29', 155, 74, 3, '2020-02-29 22:59:37', '2020-02-29 22:59:37', 168),
(146, '2020-02-29', 156, 74, 3, '2020-02-29 22:59:53', '2020-02-29 22:59:53', 169),
(147, '2020-02-29', 157, 74, 3, '2020-02-29 23:07:40', '2020-02-29 23:07:40', 170),
(148, '2020-02-29', 158, 74, 3, '2020-02-29 23:07:56', '2020-02-29 23:07:56', 171),
(149, '2020-02-29', 159, 74, 3, '2020-02-29 23:08:00', '2020-02-29 23:08:00', 172),
(150, '2020-02-29', 160, 74, 3, '2020-02-29 23:08:14', '2020-02-29 23:08:14', 173),
(151, '2020-02-29', 161, 74, 3, '2020-02-29 23:08:16', '2020-02-29 23:08:16', 174),
(152, '2020-02-29', 162, 74, 3, '2020-02-29 23:08:24', '2020-02-29 23:08:24', 175),
(153, '2020-02-29', 163, 74, 3, '2020-02-29 23:08:39', '2020-02-29 23:08:39', 176),
(154, '2020-03-09', 164, 75, 3, '2020-03-09 21:07:50', '2020-03-09 21:07:50', 177),
(155, '2020-03-09', 165, 75, 3, '2020-03-09 21:08:07', '2020-03-09 21:08:07', 178),
(156, '2020-03-09', 166, 75, 3, '2020-03-09 21:08:26', '2020-03-09 21:08:26', 179),
(157, '2020-03-09', 167, 75, 3, '2020-03-09 21:10:27', '2020-03-09 21:10:27', 180),
(158, '2020-03-09', 168, 75, 3, '2020-03-09 21:12:50', '2020-03-09 21:12:50', 181),
(159, '2020-03-09', 169, 75, 3, '2020-03-09 21:14:09', '2020-03-09 21:14:09', 182),
(160, '2020-03-09', 170, 75, 3, '2020-03-09 21:14:12', '2020-03-09 21:14:12', 183),
(161, '2020-03-09', 171, 75, 3, '2020-03-09 21:14:20', '2020-03-09 21:14:20', 184),
(162, '2020-03-09', 172, 75, 3, '2020-03-09 21:14:42', '2020-03-09 21:14:42', 185),
(163, '2020-03-12', 173, 69, 3, '2020-03-12 23:51:15', '2020-03-12 23:51:15', 186),
(164, '2021-02-21', 174, 76, 3, '2021-02-21 22:35:17', '2021-02-21 22:35:17', 187),
(165, '2021-02-21', 175, 76, 3, '2021-02-21 22:35:29', '2021-02-21 22:35:29', 188),
(166, '2021-02-21', 176, 76, 3, '2021-02-21 22:36:11', '2021-02-21 22:36:11', 189),
(167, '2021-02-21', 177, 76, 3, '2021-02-21 22:36:50', '2021-02-21 22:36:50', 190),
(168, '2021-02-21', 178, 76, 3, '2021-02-21 22:37:00', '2021-02-21 22:37:00', 191),
(169, '2021-02-21', 179, 76, 3, '2021-02-21 22:37:09', '2021-02-21 22:37:09', 192),
(170, '2021-02-21', 180, 76, 3, '2021-02-21 22:37:09', '2021-02-21 22:37:09', 193),
(171, '2021-02-21', 181, 76, 3, '2021-02-21 22:37:09', '2021-02-21 22:37:09', 194),
(172, '2021-02-21', 182, 76, 3, '2021-02-21 22:37:09', '2021-02-21 22:37:09', 195),
(173, '2021-02-21', 183, 76, 3, '2021-02-21 22:37:24', '2021-02-21 22:37:24', 196),
(174, '2021-02-21', 184, 76, 3, '2021-02-21 22:37:33', '2021-02-21 22:37:33', 197),
(175, '2021-02-21', 185, 76, 3, '2021-02-21 22:37:42', '2021-02-21 22:37:42', 198),
(176, '2021-02-21', 186, 76, 3, '2021-02-21 22:37:51', '2021-02-21 22:37:51', 199),
(177, '2021-02-21', 187, 69, 3, '2021-02-21 22:39:15', '2021-02-21 22:39:15', 200),
(178, '2021-02-21', 188, 69, 3, '2021-02-21 22:39:17', '2021-02-21 22:39:17', 201),
(179, '2021-02-21', 189, 69, 3, '2021-02-21 22:39:23', '2021-02-21 22:39:23', 202),
(180, '2021-02-21', 190, 69, 3, '2021-02-21 22:39:40', '2021-02-21 22:39:40', 203),
(181, '2021-02-21', 191, 69, 3, '2021-02-21 22:39:45', '2021-02-21 22:39:45', 204),
(182, '2021-02-23', 192, 77, 3, '2021-02-23 01:49:03', '2021-02-23 01:49:03', 205),
(183, '2021-02-23', 193, 77, 3, '2021-02-23 01:49:50', '2021-02-23 01:49:50', 206),
(184, '2021-02-23', 194, 77, 3, '2021-02-23 01:50:14', '2021-02-23 01:50:14', 207),
(185, '2021-02-23', 195, 77, 3, '2021-02-23 02:37:47', '2021-02-23 02:37:47', 208),
(186, '2021-02-25', 196, 78, 3, '2021-02-25 09:56:30', '2021-02-25 09:56:30', 209),
(187, '2021-02-25', 197, 78, 3, '2021-02-25 09:56:31', '2021-02-25 09:56:31', 210),
(188, '2021-02-25', 198, 78, 3, '2021-02-25 09:56:31', '2021-02-25 09:56:31', 211),
(189, '2021-02-25', 199, 78, 3, '2021-02-25 09:57:20', '2021-02-25 09:57:20', 212);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `empleados_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `created_at`, `updated_at`, `empleados_id`) VALUES
(3, 'Administrador', 'admin@admin.com', '$2y$10$2S5CsDnTpb7KJRGlnrxVI.MPOEt3jv.qbMqHIycYqKx9bDdFUpudu', NULL, NULL, 1),
(4, 'Andres', 'andres@gmail.com', '$2y$10$UC/to3BozStfCHbHDxxeUuNrsQVAveheBbZ1ar.pqx5.AC9PuGHiO', '2019-08-29 06:38:01', '2019-08-29 06:38:01', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_has_rol`
--

CREATE TABLE `user_has_rol` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `rol_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `acompañantes`
--
ALTER TABLE `acompañantes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_acompañantes_generos1_idx` (`generos_id`);

--
-- Indices de la tabla `alergias`
--
ALTER TABLE `alergias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `alergia_triaje`
--
ALTER TABLE `alergia_triaje`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_alergia_triaje_alergias1_idx` (`alergias_id`),
  ADD KEY `fk_alergia_triaje_triaje1_idx` (`triaje_id`);

--
-- Indices de la tabla `antecpatologicos`
--
ALTER TABLE `antecpatologicos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_antecpatologicos_triaje1_idx` (`triaje_id`);

--
-- Indices de la tabla `antecpersonals`
--
ALTER TABLE `antecpersonals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_antecpersonals_ps1_idx` (`ps_id`),
  ADD KEY `fk_antecpersonals_manticonceotivos1_idx` (`manticonceotivos_id`);

--
-- Indices de la tabla `apetitos`
--
ALTER TABLE `apetitos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cies`
--
ALTER TABLE `cies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cies_tipocies1_idx` (`tipocies_id`);

--
-- Indices de la tabla `citas`
--
ALTER TABLE `citas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_citas_pacientes_idx` (`pacientes_id`),
  ADD KEY `fk_citas_comprobante1_idx` (`comprobante_id`),
  ADD KEY `fk_citas_acompañantes1_idx` (`acompañantes_id`),
  ADD KEY `fk_citas_users1_idx` (`users_id`);

--
-- Indices de la tabla `cita_exaginecologicos`
--
ALTER TABLE `cita_exaginecologicos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cita_exaginecologicos_exaginecologicos1_idx` (`exaginecologicos_id`),
  ADD KEY `fk_cita_exaginecologicos_citas1_idx` (`citas_id`);

--
-- Indices de la tabla `cita_motivo`
--
ALTER TABLE `cita_motivo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cita_motivo_citas1_idx` (`citas_id`),
  ADD KEY `fk_cita_motivo_tipoexaminters1_idx` (`tipoexaminters_id`);

--
-- Indices de la tabla `comprobantes`
--
ALTER TABLE `comprobantes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `connsulta_exaginecologico`
--
ALTER TABLE `connsulta_exaginecologico`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_connsulta_exaginecologico_consultas1_idx` (`consultas_id`),
  ADD KEY `fk_connsulta_exaginecologico_exaginecologicos1_idx` (`exaginecologicos_id`);

--
-- Indices de la tabla `consultas`
--
ALTER TABLE `consultas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_consultas_citas1_idx` (`citas_id`),
  ADD KEY `fk_consultas_funcbiologicas1_idx` (`funcbiologicas_id`);

--
-- Indices de la tabla `consulta_diagnostico`
--
ALTER TABLE `consulta_diagnostico`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_consulta_diagnostico_consultas1_idx` (`consultas_id`);

--
-- Indices de la tabla `deposicions`
--
ALTER TABLE `deposicions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `diagnostico_cies`
--
ALTER TABLE `diagnostico_cies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_diagnostico_cies_cies1_idx` (`cies_id`),
  ADD KEY `fk_diagnostico_cies_consultas1_idx` (`consultas_id`);

--
-- Indices de la tabla `documentos`
--
ALTER TABLE `documentos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_empleados_tipoempleados1_idx` (`tipoempleados_id`),
  ADD KEY `fk_empleados_generos1_idx` (`generos_id`);

--
-- Indices de la tabla `estadocivils`
--
ALTER TABLE `estadocivils`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `exaexterno_ordenexam`
--
ALTER TABLE `exaexterno_ordenexam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_exaexterno_ordenexam_ordenexam1_idx` (`ordenexam_id`),
  ADD KEY `fk_exaexterno_ordenexam_examenexterno1_idx` (`examenexterno_id`);

--
-- Indices de la tabla `exaginecologicos`
--
ALTER TABLE `exaginecologicos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `examenexterno`
--
ALTER TABLE `examenexterno`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_examenExterno_tipoexamexterno1_idx` (`tipoexamexterno_id`);

--
-- Indices de la tabla `exauxresult`
--
ALTER TABLE `exauxresult`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_exauxresult_cita_motivo1_idx` (`cita_motivo_id`);

--
-- Indices de la tabla `funcbiologicas`
--
ALTER TABLE `funcbiologicas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_funcbiologicas_apetitos1_idx` (`apetitos_id`),
  ADD KEY `fk_funcbiologicas_deposicions1_idx` (`deposicions_id`),
  ADD KEY `fk_funcbiologicas_seds1_idx` (`seds_id`),
  ADD KEY `fk_funcbiologicas_sueños1_idx` (`sueños_id`),
  ADD KEY `fk_funcbiologicas_orinas1_idx` (`orinas_id`);

--
-- Indices de la tabla `funcvitals`
--
ALTER TABLE `funcvitals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_funcvitals_presarterials1_idx` (`presarterials_id`);

--
-- Indices de la tabla `generos`
--
ALTER TABLE `generos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `insgenerals`
--
ALTER TABLE `insgenerals`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `insgeneral_consulta`
--
ALTER TABLE `insgeneral_consulta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_insgeneral_consulta_insgenerals1_idx` (`insgenerals_id`),
  ADD KEY `fk_insgeneral_consulta_consultas1_idx` (`consultas_id`);

--
-- Indices de la tabla `instruccions`
--
ALTER TABLE `instruccions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `laboratorios`
--
ALTER TABLE `laboratorios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `manticonceptivos`
--
ALTER TABLE `manticonceptivos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `medicamentos`
--
ALTER TABLE `medicamentos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_model_has_permissions_permissions1_idx` (`permissions_id`);

--
-- Indices de la tabla `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_model_has_roles_roles1_idx` (`role_id`);

--
-- Indices de la tabla `observacions`
--
ALTER TABLE `observacions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_observacions_consultas1_idx` (`consultas_id`);

--
-- Indices de la tabla `ocupacions`
--
ALTER TABLE `ocupacions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ordenexam`
--
ALTER TABLE `ordenexam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ordenexam_consultas1_idx` (`consultas_id`);

--
-- Indices de la tabla `orinas`
--
ALTER TABLE `orinas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pacientes`
--
ALTER TABLE `pacientes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pacientes_ocupacions1_idx` (`ocupacions_id`),
  ADD KEY `fk_pacientes_instruccions1_idx` (`instruccions_id`),
  ADD KEY `fk_pacientes_estadocivils1_idx` (`estadocivils_id`),
  ADD KEY `fk_pacientes_generos1_idx` (`generos_id`);

--
-- Indices de la tabla `parentezcos`
--
ALTER TABLE `parentezcos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_parentezco_acompañantes1_idx` (`acompañantes_id`),
  ADD KEY `fk_parentezco_pacientes1_idx` (`pacientes_id`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `presarterials`
--
ALTER TABLE `presarterials`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ps`
--
ALTER TABLE `ps`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `resultadoexamen`
--
ALTER TABLE `resultadoexamen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_resultadoexamen_laboratorios1_idx` (`laboratorios_id`),
  ADD KEY `fk_resultadoexamen_ordenexam1_idx` (`ordenexam_id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_permiso_rol_rols1_idx` (`role_id`),
  ADD KEY `fk_permiso_rol_permisos1_idx` (`permission_id`);

--
-- Indices de la tabla `seds`
--
ALTER TABLE `seds`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sueños`
--
ALTER TABLE `sueños`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipocies`
--
ALTER TABLE `tipocies`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipoempleados`
--
ALTER TABLE `tipoempleados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipoexamexterno`
--
ALTER TABLE `tipoexamexterno`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipoexaminters`
--
ALTER TABLE `tipoexaminters`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tratamientos`
--
ALTER TABLE `tratamientos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tratamientos_consultas1_idx` (`consultas_id`),
  ADD KEY `fk_tratamientos_medicamentos1_idx` (`medicamentos_id`);

--
-- Indices de la tabla `triaje`
--
ALTER TABLE `triaje`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_triaje_antecpersonals1_idx` (`antecpersonals_id`),
  ADD KEY `fk_triaje_citas1_idx` (`citas_id`),
  ADD KEY `fk_triaje_users1_idx` (`users_id`),
  ADD KEY `fk_triaje_funcvitals1_idx` (`funcvitals_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_empleados1_idx` (`empleados_id`);

--
-- Indices de la tabla `user_has_rol`
--
ALTER TABLE `user_has_rol`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_rol_users1_idx` (`users_id`),
  ADD KEY `fk_user_rol_rols1_idx` (`rol_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `acompañantes`
--
ALTER TABLE `acompañantes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `alergias`
--
ALTER TABLE `alergias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de la tabla `alergia_triaje`
--
ALTER TABLE `alergia_triaje`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT de la tabla `antecpatologicos`
--
ALTER TABLE `antecpatologicos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT de la tabla `antecpersonals`
--
ALTER TABLE `antecpersonals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=200;

--
-- AUTO_INCREMENT de la tabla `apetitos`
--
ALTER TABLE `apetitos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `cies`
--
ALTER TABLE `cies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT de la tabla `citas`
--
ALTER TABLE `citas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT de la tabla `cita_exaginecologicos`
--
ALTER TABLE `cita_exaginecologicos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cita_motivo`
--
ALTER TABLE `cita_motivo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT de la tabla `comprobantes`
--
ALTER TABLE `comprobantes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `connsulta_exaginecologico`
--
ALTER TABLE `connsulta_exaginecologico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consultas`
--
ALTER TABLE `consultas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT de la tabla `consulta_diagnostico`
--
ALTER TABLE `consulta_diagnostico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `deposicions`
--
ALTER TABLE `deposicions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `diagnostico_cies`
--
ALTER TABLE `diagnostico_cies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT de la tabla `documentos`
--
ALTER TABLE `documentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `empleados`
--
ALTER TABLE `empleados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `estadocivils`
--
ALTER TABLE `estadocivils`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `exaexterno_ordenexam`
--
ALTER TABLE `exaexterno_ordenexam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT de la tabla `exaginecologicos`
--
ALTER TABLE `exaginecologicos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT de la tabla `examenexterno`
--
ALTER TABLE `examenexterno`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=327;

--
-- AUTO_INCREMENT de la tabla `exauxresult`
--
ALTER TABLE `exauxresult`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `funcbiologicas`
--
ALTER TABLE `funcbiologicas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT de la tabla `funcvitals`
--
ALTER TABLE `funcvitals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=213;

--
-- AUTO_INCREMENT de la tabla `generos`
--
ALTER TABLE `generos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `insgenerals`
--
ALTER TABLE `insgenerals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `insgeneral_consulta`
--
ALTER TABLE `insgeneral_consulta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;

--
-- AUTO_INCREMENT de la tabla `instruccions`
--
ALTER TABLE `instruccions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `laboratorios`
--
ALTER TABLE `laboratorios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `manticonceptivos`
--
ALTER TABLE `manticonceptivos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `medicamentos`
--
ALTER TABLE `medicamentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `model_has_roles`
--
ALTER TABLE `model_has_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `observacions`
--
ALTER TABLE `observacions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `ocupacions`
--
ALTER TABLE `ocupacions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `ordenexam`
--
ALTER TABLE `ordenexam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `orinas`
--
ALTER TABLE `orinas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `pacientes`
--
ALTER TABLE `pacientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `parentezcos`
--
ALTER TABLE `parentezcos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `presarterials`
--
ALTER TABLE `presarterials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=237;

--
-- AUTO_INCREMENT de la tabla `ps`
--
ALTER TABLE `ps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=213;

--
-- AUTO_INCREMENT de la tabla `resultadoexamen`
--
ALTER TABLE `resultadoexamen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sueños`
--
ALTER TABLE `sueños`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `tipocies`
--
ALTER TABLE `tipocies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipoempleados`
--
ALTER TABLE `tipoempleados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tipoexamexterno`
--
ALTER TABLE `tipoexamexterno`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `tipoexaminters`
--
ALTER TABLE `tipoexaminters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `tratamientos`
--
ALTER TABLE `tratamientos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `triaje`
--
ALTER TABLE `triaje`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=190;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `user_has_rol`
--
ALTER TABLE `user_has_rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `acompañantes`
--
ALTER TABLE `acompañantes`
  ADD CONSTRAINT `fk_acompañantes_generos1` FOREIGN KEY (`generos_id`) REFERENCES `generos` (`id`);

--
-- Filtros para la tabla `alergia_triaje`
--
ALTER TABLE `alergia_triaje`
  ADD CONSTRAINT `fk_alergia_triaje_alergias1` FOREIGN KEY (`alergias_id`) REFERENCES `alergias` (`id`),
  ADD CONSTRAINT `fk_alergia_triaje_triaje1` FOREIGN KEY (`triaje_id`) REFERENCES `triaje` (`id`);

--
-- Filtros para la tabla `antecpatologicos`
--
ALTER TABLE `antecpatologicos`
  ADD CONSTRAINT `fk_antecpatologicos_triaje1` FOREIGN KEY (`triaje_id`) REFERENCES `triaje` (`id`);

--
-- Filtros para la tabla `antecpersonals`
--
ALTER TABLE `antecpersonals`
  ADD CONSTRAINT `fk_antecpersonals_manticonceotivos1` FOREIGN KEY (`manticonceotivos_id`) REFERENCES `manticonceptivos` (`id`),
  ADD CONSTRAINT `fk_antecpersonals_ps1` FOREIGN KEY (`ps_id`) REFERENCES `ps` (`id`);

--
-- Filtros para la tabla `cies`
--
ALTER TABLE `cies`
  ADD CONSTRAINT `fk_cies_tipocies1` FOREIGN KEY (`tipocies_id`) REFERENCES `tipocies` (`id`);

--
-- Filtros para la tabla `citas`
--
ALTER TABLE `citas`
  ADD CONSTRAINT `fk_citas_acompañantes1` FOREIGN KEY (`acompañantes_id`) REFERENCES `acompañantes` (`id`),
  ADD CONSTRAINT `fk_citas_comprobante1` FOREIGN KEY (`comprobante_id`) REFERENCES `comprobantes` (`id`),
  ADD CONSTRAINT `fk_citas_pacientes` FOREIGN KEY (`pacientes_id`) REFERENCES `pacientes` (`id`),
  ADD CONSTRAINT `fk_citas_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `cita_exaginecologicos`
--
ALTER TABLE `cita_exaginecologicos`
  ADD CONSTRAINT `fk_cita_exaginecologicos_citas1` FOREIGN KEY (`citas_id`) REFERENCES `citas` (`id`),
  ADD CONSTRAINT `fk_cita_exaginecologicos_exaginecologicos1` FOREIGN KEY (`exaginecologicos_id`) REFERENCES `exaginecologicos` (`id`);

--
-- Filtros para la tabla `connsulta_exaginecologico`
--
ALTER TABLE `connsulta_exaginecologico`
  ADD CONSTRAINT `fk_connsulta_exaginecologico_consultas1` FOREIGN KEY (`consultas_id`) REFERENCES `consultas` (`id`),
  ADD CONSTRAINT `fk_connsulta_exaginecologico_exaginecologicos1` FOREIGN KEY (`exaginecologicos_id`) REFERENCES `exaginecologicos` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
